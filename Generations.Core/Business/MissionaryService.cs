﻿namespace Generations.Core.Business
{
    using System;
    using System.Collections.Generic;
    using Generations.Core.Business.Objects;
    using Generations.Core.Data;

    //using Generations.Core.Data;

    //using Generations.Core.Data;

    public class MissionaryService : IMissionaryService
    {
        IMissionaryRepository _repository;
        public MissionaryService(IMissionaryRepository repository)
        {
            this._repository = repository;
        }

        public MissionaryService() : this (new EFMissionaryRepository())
        {            
        }

        public IEnumerable<Missionary> GetAllMissionaries()
        {
            return _repository.GetAllMissionaries();
        }

        public IEnumerable<Missionary> GetAllRootMissionaries()
        {
            return _repository.GetAllRootMissionaries();
        }

        public Missionary GetMissionary(int missionaryId)
        {
            return _repository.GetMissionary(missionaryId);
        }

        public IEnumerable<Missionary> GetMissionaryChildren(int missionaryId)
        {
            return _repository.GetMissionaryChildren(missionaryId);
        }

        public bool CreateMissionary(Missionary missionary)
        {
            return _repository.CreateMissionary(missionary);
        }

        public bool UpdateMissionary(Missionary missionary)
        {
            return _repository.UpdateMissionary(missionary);
        }

        public bool DeleteMissionary(int missionaryId)
        {
            return _repository.DeleteMissionary(missionaryId);
        }
    }
}
