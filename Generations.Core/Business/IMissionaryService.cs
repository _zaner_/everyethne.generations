﻿using System.Collections.Generic;
using Generations.Core.Business.Objects;

namespace Generations.Core.Business
{
    public interface IMissionaryService
    {
        IEnumerable<Missionary> GetAllMissionaries();
        IEnumerable<Missionary> GetAllRootMissionaries();
        Missionary GetMissionary(int missionaryId);
        IEnumerable<Missionary> GetMissionaryChildren(int missionaryId);
        bool CreateMissionary(Missionary missionary);
        bool UpdateMissionary(Missionary missionary);
        bool DeleteMissionary(int missionaryId);
    }
}
