﻿namespace Generations.Core.Business.Objects 
{
    using System.Collections.Generic;

    public class Church 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Missionary Missionary { get; set; }
        public string LeaderName { get; set; }
        public string LeaderAddress { get; set; }
        public string LeaderEmail { get; set; }
        public string LeaderPhone { get; set; }
        public int Size { get; set; }
        public IEnumerable<Church> Children { get; set; }
        public Church Parent { get; set; }
    }
}
