﻿namespace Generations.Core.Business.Objects
{
    using System;
    using System.Collections.Generic;
    
    public class Missionary
    {
        public int Id { get; set; }
        public int? ParentMissionaryId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string ImageLocation { get; set; }
        public bool IsEveryEthneStaff { get; set; }
        public string AdditionalInfo { get; set; }
        public IEnumerable<Missionary> Children { get; set; }

        // Removing this from the class since the logic is not needed, however I want to preserve it in case I use a state in the future
        //public string StateName
        //{
        //    get
        //    {
        //        return Enum.GetName(typeof(MissionaryState), State);
        //    }
        //}
    }
}
