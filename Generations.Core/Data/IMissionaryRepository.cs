﻿namespace Generations.Core.Data
{
    using System.Collections.Generic;

    using Generations.Core.Business.Objects;

    public interface IMissionaryRepository
    {
        IEnumerable<Missionary> GetAllMissionaries();
        IEnumerable<Missionary> GetAllRootMissionaries();
        Missionary GetMissionary(int missionaryId);
        IEnumerable<Missionary> GetMissionaryChildren(int missionaryId);
        bool CreateMissionary(Missionary missionary);
        bool UpdateMissionary(Missionary missionary);
        bool DeleteMissionary(int missionaryId);
    }
}
