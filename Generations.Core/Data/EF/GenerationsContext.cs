﻿namespace Generations.Core.Data.EF
{
    using System.Data.Entity;
    using Generations.Core.Business.Objects;
    using Generations.Core.Data.EF.Configurations;

    public class GenerationsContext : DbContext
    {
        public DbSet<Missionary> Missionaries { get; set; }

        public GenerationsContext(string connectionString) : base(connectionString)
        {
            Database.SetInitializer<GenerationsContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new MissionaryConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
