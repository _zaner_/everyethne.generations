﻿namespace Generations.Core.Data.EF.Configurations
{
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity.ModelConfiguration;

    using Generations.Core.Business.Objects;

    public class MissionaryConfiguration : EntityTypeConfiguration<Missionary>
    {
        public MissionaryConfiguration()
        {
            // Setup Keys
            HasKey(p => p.Id);

            // Setup Columns
            Property(p => p.Id)
                .HasColumnName("MissionaryId")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();
            Property(p => p.ParentMissionaryId)
                .HasColumnName("ParentMissionaryId")
                .IsOptional();
            Property(p => p.Name)
                .HasColumnName("Name")
                .IsRequired();
            Property(p => p.Email)
                .HasColumnName("Email")
                .IsOptional();
            Property(p => p.Phone)
                .HasColumnName("Phone")
                .IsOptional();
            Property(p => p.Address)
                .HasColumnName("Address")
                .IsOptional();
            Property(p => p.ImageLocation)
                .HasColumnName("ImageLocation")
                .IsOptional();
            Property(p => p.IsEveryEthneStaff)
                .HasColumnName("IsEveryEthneStaff")
                .IsOptional();
            Property(p => p.AdditionalInfo)
                .HasColumnName("AdditionalInfo")
                .IsOptional();

            // Table Options            
            ToTable("Missionaries");
        }
    }
}
