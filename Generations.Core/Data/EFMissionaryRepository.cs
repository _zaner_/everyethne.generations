﻿namespace Generations.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using Generations.Core.Business.Objects;
    using Generations.Core.Data.EF;

    public class EFMissionaryRepository : IMissionaryRepository
    {
        private GenerationsContext _context;
        public EFMissionaryRepository()
        {
            _context = new GenerationsContext(ConfigurationManager.ConnectionStrings["Generations"].ConnectionString);
        }

        public IEnumerable<Missionary> GetAllMissionaries()
        {
            return _context.Missionaries.ToList();
        }

        public IEnumerable<Missionary> GetAllRootMissionaries()
        {
            return _context.Missionaries.Where(x => x.ParentMissionaryId == null);
        }

        public Missionary GetMissionary(int missionaryId)
        {
            return _context.Missionaries.SingleOrDefault(x => x.Id == missionaryId);
        }

        public IEnumerable<Missionary> GetMissionaryChildren(int missionaryId)
        {
            return _context.Missionaries.Where(y => y.ParentMissionaryId == missionaryId).ToList();
        }

        public bool CreateMissionary(Missionary missionary)
        {
            _context.Missionaries.Add(missionary);
            try
            {
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateMissionary(Missionary missionary)
        {            
            try
            {
                var contextMissionary = _context.Missionaries.FirstOrDefault(m => m.Id == missionary.Id);
                contextMissionary.AdditionalInfo = missionary.AdditionalInfo;
                contextMissionary.Address = missionary.Address;
                contextMissionary.ImageLocation = missionary.ImageLocation;
                contextMissionary.IsEveryEthneStaff = missionary.IsEveryEthneStaff;
                contextMissionary.Name = missionary.Name;
                contextMissionary.Phone = missionary.Phone;

                _context.SaveChanges();  
                return true;
            } catch (Exception) {
                return false;
            }
        }

        public bool DeleteMissionary(int missionaryId)
        {
            try
            {
                // Recurse Through Children and Remove
                var missionary = _context.Missionaries.FirstOrDefault(m => m.Id == missionaryId);                
                RemoveMissionaryAndChildrenRecursive(missionary);
                return true;
            } catch (Exception)
            {
                return false;
            }
        }

        private void RemoveMissionaryAndChildrenRecursive(Missionary missionary)
        {
            missionary.Children = GetMissionaryChildren(missionary.Id);
            foreach (var child in missionary.Children)
            {
                RemoveMissionaryAndChildrenRecursive(child);
            }
            _context.Missionaries.Remove(missionary);
            _context.SaveChanges();
        }
    }
}
