﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Generations.MVC.Models.Validators
{
    using System.ComponentModel.DataAnnotations;
    using System.Text.RegularExpressions;

    public class PhoneAttribute : ValidationAttribute
    {
        public PhoneAttribute() : base("You must provide a valid US phone number with area code.") {}

        public override bool IsValid(object value)
        {
            // This is not a required field, so don't validate on empty
            if (value == null)
                return true;

            // Valid US phone numbers: 1-205-123-4567, 205-123-4567 (any special can be used for delimiting)
            var patternSpecials = "(?:[^0-9 ]|(?<=['\"])s)";
            var regex = new Regex(patternSpecials);
            var phoneWithoutSpecials = regex.Replace((string)value, string.Empty);

            // If number is less that standard number with area code (205-123-4567 = 10 digits)
            // OR If number is greater than number with usa international code (1-205-123-4567 = 11 digits)
            if (phoneWithoutSpecials.Length < 10 || phoneWithoutSpecials.Length > 11)
                return false;

            // Verify first number in 11 digit sequence is 1
            if (phoneWithoutSpecials.Length == 11 && phoneWithoutSpecials[0] != 1)
                return false;

            return true;
        }
    }
}