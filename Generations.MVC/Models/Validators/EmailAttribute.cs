﻿namespace Generations.MVC.Models.Validators
{
    using System.ComponentModel.DataAnnotations;
    using System.Text.RegularExpressions;

    public class EmailAttribute : ValidationAttribute
    {
        public EmailAttribute() : base("Your email address in invalid.") { }

        public override bool IsValid(object value)
        {
            // This is not a required field, so don't validate on empty
            if (value == null)
                return true;

            string patternStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
                                   + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                                   + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                                   + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                                   + @"[a-zA-Z]{2,}))$";
            
            var regex = new Regex(patternStrict);

            if (!regex.IsMatch((string)value))
                return false;

            return true;
        }
    }
}