﻿namespace Generations.MVC.Models.Validators
{
    using System.ComponentModel.DataAnnotations;

    public class AdditionalInfoAttribute : ValidationAttribute
    {
        public AdditionalInfoAttribute() : base("You can only have a maximum of 500 characters for this field.") { }

        public override bool  IsValid(object value)
        {
            // This is not a required field, so don't validate on empty
            if (value == null)
                return true;

            var stringValue = (string)value;
            if (stringValue.Length > 500)
                return false;

            return true;
        }
    }
}