﻿namespace Generations.MVC.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using Generations.Core.Business.Objects;
    using Generations.MVC.Models.Validators;

    public class MissionaryTreeModel
    {
        public Missionary Missionary { get; set; }   
    }

    public class MissionaryListModel
    {
        public List<Missionary> Missionaries { get; set; }    
    }

    public class MissionaryCrudModel
    {
        public int Id { get; set; }
        public int? ParentMissionaryId { get; set; }
        [Required(ErrorMessage = "You must provide a name.")]
        public string Name { get; set; }
        [Email]
        public string Email { get; set; }
        [Phone]
        public string Phone { get; set; }
        public string Address { get; set; }
        [AdditionalInfo]
        public string AdditionalInfo { get; set; }
        public string ImageLocation { get; set; }
        public bool IsEveryEthneStaff { get; set; }
        public string ErrorMessage { get; set; }
    }
}