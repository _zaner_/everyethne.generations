﻿using System.Web.Mvc;

namespace Generations.MVC.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Missionary");
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
