﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using Generations.Core.Business;
using Generations.Core.Business.Objects;
using Generations.Core.Data;
using Generations.MVC.Models;

namespace Generations.MVC.Controllers
{

    [Authorize]
    public class MissionaryController : Controller
    {
        private MissionaryService _service;
        public MissionaryController(MissionaryService service)
        {
            _service = service;   
        }

        public MissionaryController() : this (new MissionaryService(new EFMissionaryRepository())) {}

        //
        // GET: /Missionaries/
        public ActionResult Index()
        {
            var model = new MissionaryListModel { Missionaries = this._service.GetAllRootMissionaries().ToList() };
            return View(model);
        }

        //
        // Get: /Missionary/Tree/MissionaryIdOrName
        public ActionResult Tree(int id)
        {
            var missionary = _service.GetMissionary(id);
            PopulateMissionaryChildrenRecursiveControlled(missionary);
            
            return View(new MissionaryTreeModel { Missionary = missionary });
        }

        //
        // GET: /Missionaries/Create
        [Authorize(Roles = "Administrator,Contributor")]
        public ActionResult Create(int? id)
        {
            return View(new MissionaryCrudModel());
        } 

        //
        // POST: /Missionaries/Create
        [HttpPost]
        [Authorize(Roles = "Administrator,Contributor")]
        public ActionResult Create(int? id, MissionaryCrudModel model, HttpPostedFileBase imageFile)
        {            
            // Verify Model
            if (!ModelState.IsValid)
            {
                return View(model);    
            }

            var missionary = new Missionary
            {
                Name = model.Name,
                Address = model.Address,
                Email = model.Email,
                IsEveryEthneStaff = model.IsEveryEthneStaff,
                ParentMissionaryId = id,
                Phone = model.Phone
            };

            // Verify Image Selected            
            if (imageFile != null && imageFile.ContentLength > 0)
            {
                // Upload Image
                var uploadResult = UploadImage(imageFile);
                if (uploadResult[uploadResult.Length - 1] == '!')
                {
                    model.ErrorMessage = uploadResult;
                    return View(model);
                }

                missionary.ImageLocation = uploadResult;
            }
            
            _service.CreateMissionary(missionary);

            if (id != null)
                return RedirectToAction("Tree", new { id });

            return RedirectToAction("Index");
        }
        
        //
        // GET: /Missionaries/Edit/5
        [Authorize(Roles = "Administrator,Contributor")]
        public ActionResult Edit(int id)
        {
            var missionary = _service.GetMissionary(id);
            if (missionary == null)
                return View(new MissionaryCrudModel { ErrorMessage = "Missionary does not exist!" });

            var crudModel = new MissionaryCrudModel
                                {
                                    AdditionalInfo = missionary.AdditionalInfo,
                                    Address = missionary.Address,
                                    Email = missionary.Email,
                                    Id = id,
                                    ImageLocation = missionary.ImageLocation,
                                    IsEveryEthneStaff = missionary.IsEveryEthneStaff,
                                    Name = missionary.Name,
                                    ParentMissionaryId = missionary.ParentMissionaryId,
                                    Phone = missionary.Phone
                                };

            return View(crudModel);
        }

        //
        // POST: /Missionaries/Edit/5
        [HttpPost]
        [Authorize(Roles = "Administrator,Contributor")]
        public ActionResult Edit(int id, MissionaryCrudModel model, HttpPostedFileBase imageFile)
        {
            // Verify Model
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var missionary = new Missionary
            {
                Id = model.Id,
                Name = model.Name,
                Address = model.Address,
                Email = model.Email,
                IsEveryEthneStaff = model.IsEveryEthneStaff,
                Phone = model.Phone
            };

            // Verify Image Selected            
            if (imageFile != null && imageFile.ContentLength > 0)
            {
                // Upload Image
                var uploadResult = UploadImage(imageFile);
                if (uploadResult[uploadResult.Length - 1] == '!')
                {
                    model.ErrorMessage = uploadResult;
                    return View(model);
                }

                missionary.ImageLocation = uploadResult;
            }

            _service.UpdateMissionary(missionary);

            return RedirectToAction("Tree", new { id });
        }

        //
        // GET: /Missionaries/Edit/5/1
        [Authorize(Roles = "Administrator,Contributor")]
        public ActionResult Delete(int id, int? parentId)
        {
            try
            {
                _service.DeleteMissionary(id);

                // Delete Images

                if (parentId != null)
                    return RedirectToAction("Tree", new { id = parentId });

                return RedirectToAction("Index");
            }
            catch
            {
                var model = new MissionaryCrudModel();
                model.ErrorMessage = "An error occured while trying to delete!";
                return RedirectToAction("Edit", new { model });
            }
        }

        #region Helper Methods
        

        private void PopulateMissionaryChildrenRecursiveControlled(Missionary missionary)
        {
            missionary.Children = _service.GetMissionaryChildren(missionary.Id);
            foreach (var child in missionary.Children)
            {
                this.PopulateMissionaryChildrenRecursiveControlled(child);
            }
        }

        #region Image Upload

        // Return string specifies error message if encountered, or new image file path
        private string UploadImage(HttpPostedFileBase imageFile)
        {            
            // Verify Files Size
            if (!this.VerifyImageFileSize(imageFile.ContentLength)) return "File was too large!";

            // Verify File Type
            var fileName = Path.GetFileName(imageFile.FileName).ToLower();
            if (!VerifyImageFileType(fileName)) return "Unsupported File Type!";

            // Upload to Local
            var fileNamePath = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
            imageFile.SaveAs(fileNamePath);

            // Resize Image and Save
            var resizedFileName = ResizeFileAndSave(fileNamePath);

            // Upload to Storage

            // Set Path Metadata

            // Clean Up Local Upload Path

            return resizedFileName;
        }

        private bool VerifyImageFileSize(int size)
        {
            // Get Configured Permitted Size
            var permittedSize = int.Parse(ConfigurationManager.AppSettings["ImageUploadMaxByteSize"]);
            if (size <= permittedSize)
                return true;

            return false;
        }

        private bool VerifyImageFileType(string fileName)
        {
            var extension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

            // Get Configured Permitted File Types
            List<string> fileTypes = ConfigurationManager.AppSettings["ImageUploadPermittedFileTypes"].Split(',').ToList();
            if (fileTypes.Exists(x => x.Equals(extension)))
                return true;

            return false;        
        }

        private string ResizeFileAndSave(string fileName)
        {
            try
            {
                var newFilePath = fileName.Insert(fileName.LastIndexOf('.'), "_thumb");

                using (Image image = Image.FromFile(fileName))
                {
                    double percentChange = 300.00 / image.Width;
                    int resizedHeight = (int)Math.Round(image.Height * percentChange, 0);
                    Image resized = new Bitmap(300, resizedHeight, image.PixelFormat);
                    Graphics graphics = Graphics.FromImage(resized);
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    Rectangle rectangle = new Rectangle(0, 0, 300, resizedHeight);
                    graphics.DrawImage(image, rectangle);
                    resized.Save(newFilePath, image.RawFormat);
                }
                return newFilePath;
            }
            catch
            {
                return string.Empty;
            }            
        }

        #endregion

        #endregion
    }
}
